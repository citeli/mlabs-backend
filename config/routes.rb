Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :search, only: [:index]

  defaults format: :json do
    namespace :api do
      namespace :v1 do

        ## Parking
        post 'parking', to: 'parking#register'
        put 'parking/:id/out', to: 'parking#out'
        put 'parking/:id/pay', to: 'parking#pay'
        get 'parking/:plate', to: 'parking#history'
      end
    end
  end

end
