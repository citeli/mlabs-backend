class ParkingSerializer < ActiveModel::Serializer
    attributes  :id,
                :reservation_code,
                :plate,
                :paid,
                :left,
                :checkout_date,
                :created_at,
                :time
end
  