module Api
    module V1
        class ParkingController < ApplicationController

            def register
                parking = ::Parking.new
                parking.plate = params['plate']
                parking.save!
                render json: parking
            end

            def pay
                parking = ::Parking.find(params[:id])
                raise ::SharedKernel::ValidationError.new('Registro não encontrado') if !parking.present?

                parking.pay
                parking.save!
                render json: parking
            end

            def out
                parking = ::Parking.find(params[:id])
              
                parking.out
                parking.save!
                render json: parking

            end

            def history
                list_parking = ::Parking.where("plate = ?", params[:plate])
                render json: list_parking
            end

        end
    end
end
