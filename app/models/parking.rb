class Parking < ApplicationRecord

    validate :valid_plate
    
    before_create :set_defaults

    def set_defaults
        self.paid = false
        self.left = false
        self.reservation_code = rand(100..30000)
    end

    def pay
        self.paid = true
    end

    def out
        raise ::SharedKernel::ValidationError.new('Não foi pssível liberar. Pagamento não realizado.') if !self.paid?
        raise ::SharedKernel::ValidationError.new('Saída já registrada.') if self.left?
        self.left = true
        self.checkout_date = Time.now
    end

    def time
        if self.left == true
            t_ini = self.created_at
            t_end = self.checkout_date
            minutes = (t_end.to_i - t_ini.to_i)/60
            if minutes <= 60
                return  "#{minutes} minut(o)s"
            end
            hours = minutes / 60
            rest = minutes % 60
            return "#{hours} horas e #{rest} minutos" 
        end
    end

    private 
    
    def valid_plate
        
        raise ::SharedKernel::ValidationError.new('Placa não fornecida') if !self.plate.present?
        raise ::SharedKernel::ValidationError.new('Placa fora do padrão') unless /\A[A-Z]{3}-[0-9]{4}\z/ =~ self.plate.upcase
        
        return true        
    end

end