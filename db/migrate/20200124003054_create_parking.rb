class CreateParking < ActiveRecord::Migration[5.2]
  def change
    create_table :parkings do |t|
      t.string  :reservation_code
      t.string  :plate
      t.boolean :paid
      t.boolean :left
      t.datetime :checkout_date
      t.timestamps
    end
  end
end
