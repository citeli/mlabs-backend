require "rails_helper"

RSpec.describe ::Api::V1::ParkingController, :type => :controller do
  describe "GET history" do
    it "200 status code" do
      get :history, :params  => {plate: 'EEZ-4455'}
      expect(response.status).to eq(200)
    end
  end
end