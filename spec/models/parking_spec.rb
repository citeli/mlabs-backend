require 'rails_helper'

RSpec.describe Parking, :type => :model do
    it "é válido quando placa estiver dentro do padrão" do 
        parking = Parking.new( plate: 'AAA-1444') 
        expect(parking).to be_valid 
    end 

    it "não pode registrar saída caso não tenha realizado pagamento" do 
        parking = Parking.create( plate: 'AAA-9999') 
        parking.out
        expect(parking).to be_valid
    end 

    it "não pode registrar saída caso não tenha realizado pagamento" do 
        parking = Parking.create( plate: 'AAA-9999') 
        parking.out
        parking.valid? 
        expect(parking).not_to 
    end 
end
